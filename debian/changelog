apriltag (3.3.0-1) unstable; urgency=medium

  * New upstream release

 -- Dima Kogan <dkogan@debian.org>  Thu, 08 Sep 2022 18:56:11 -0700

apriltag (3.2.0-1) unstable; urgency=medium

  * New upstream release
    - New tag family: tag36h10

 -- Dima Kogan <dkogan@debian.org>  Sat, 05 Feb 2022 15:58:40 -0800

apriltag (3.1.7-1) unstable; urgency=medium

  * New upstream release

    - Mostly bug fixes
    - Two tag families have been removed because they didn't work well:
      - tagStandard25h7
      - tagStandard32h10

 -- Dima Kogan <dkogan@debian.org>  Fri, 26 Mar 2021 19:26:11 -0700

apriltag (0.10.0-6) unstable; urgency=medium

  * The in-tree build uses a relative RPATH, which should make the package
    reproducible

 -- Dima Kogan <dkogan@debian.org>  Fri, 20 Dec 2019 11:01:26 -0800

apriltag (0.10.0-5) unstable; urgency=medium

  * Removed python2 dependency (Closes: #942952)

 -- Dima Kogan <dkogan@debian.org>  Tue, 19 Nov 2019 13:15:14 -0800

apriltag (0.10.0-4) unstable; urgency=medium

  * Added python interface

 -- Dima Kogan <dkogan@debian.org>  Sun, 14 Jul 2019 10:48:08 -0700

apriltag (0.10.0-3) unstable; urgency=medium

  * New detector data supports big-endian architectures

 -- Dima Kogan <dkogan@debian.org>  Fri, 08 Feb 2019 23:08:03 -0800

apriltag (0.10.0-2) unstable; urgency=medium

  * Data used by the new detectors is now read from disk at
    runtime. (Closes: #921256,#920894)

 -- Dima Kogan <dkogan@debian.org>  Sun, 03 Feb 2019 17:46:09 -0800

apriltag (0.10.0-1) unstable; urgency=medium

  * New upstream release. ABI has changed, so we distribute a new
    libapriltag package

 -- Dima Kogan <dkogan@debian.org>  Mon, 31 Dec 2018 17:08:38 -0800

apriltag (0.9.8-2) unstable; urgency=medium

  * Fixed minor bug in simple demo

 -- Dima Kogan <dkogan@debian.org>  Tue, 18 Sep 2018 10:27:24 -0700

apriltag (0.9.8-1) unstable; urgency=medium

  * Initial package release (Closes: #891601)

 -- Dima Kogan <dkogan@debian.org>  Sun, 26 Aug 2018 22:04:34 -0700
